<?php


function templ_block_view_alter(&$data, $block) {
    $allBlocks = $data;

    switch ($block->region) {
        case 'videos':
            $data['content']['#markup'] = $data['content']['#markup'] . add_video_links();
            break;

        case 'homepage':
            $data['content']['#markup'] = change_markup_rows($data, $allBlocks);
            break;
    }
}


function templ_preprocess_page (&$page) {
	global $language;
	$page['type'] = '';
	if (arg(0) == 'node' && is_numeric(arg(1))) {
		$nid = arg(1);
		$node = node_load($nid);
		$page['type'] = $node->type;
	}
	$page['downloadblock'] = get_block('download_fun', 'download_form');
    
}

function templ_get_lang () {
	global $language;
	return $language->language;
}

function templ_form_constant_contact_signup_form_alter(&$form, &$form_state) {
	$form['cc_email_1']['#title'] = t('E-mail address');
	$form['cc_FirstName_1']['#title'] = t('First name');
	$form['cc_LastName_1']['#title'] = t('Surname');
	$form['cc_CompanyName_1']['#title'] = t('Company name');
	$form['cc_newsletter_lists_1']['#title'] = t('Please indicate below which newsletter you would like to receive');
	unset($form['cc_newsletter_lists_1']['#description']);
	$form['submit ']['#value'] = t('Subscribe to Growth Impulse');
	
}



function add_method_headers () {
    $html = '<div id="methodHeaders"><h2>' . t('The GrowthPartner Method') . '</h2>';
    $html .= '<p>' . t('In four clear steps, we can realise faster, smarter and more profitable growth for your business.') . '</p></div>';
    return $html;
}

function add_video_links () {
    $html = '<div class="links"><a class="meerInfos spec_links" href="#meervideos">&raquo; ' . t('More videos') . '</a>';
    $html .= '<a class="abonneren spec_links" target="_blank" href="http://www.youtube.com/groeipartner">&raquo; ' . t('Subscribe') . '</a></div>';
    return $html;
}


function change_markup_rows ($html, $allBlocks) {
    
    $nieuwsBlock = get_block('constant_contact', '1');
    $methodeBlock = get_block('views', 'homepage_blocks-block_3');
    $recenties = get_block('views', 'homepage_blocks-recentie_block');

    $html = $html['content']['#markup'];
	
	$html = str_replace('[geen]', '', $html);
    $html = str_replace('[downloads]', '<div class="image"><div id="downloadWidget"></div><a href="#" class="loadResults">' . t('Load more') . '</a></div>', $html);
    $html = str_replace('[nieuwsbrief]', '<div class="image"><div id="nieusWidget"><h3>' . t('Newsletter') . '</h3><p>' . t('Please enter your email address.') . '</p>' . drupal_render($nieuwsBlock) . '</div></div>', $html);
	
    $html = str_replace('[methode]', add_method_headers() . drupal_render($methodeBlock), $html);
    $html = str_replace('[klanten]', '<div id="recentieLijst">' . drupal_render($recenties) . '</div>' , $html);
    return $html;
}


function get_block ($module, $delta) {
    $block = block_load($module, $delta);
    $renderable_block = _block_get_renderable_array(_block_render_blocks(array($block)));

    return $renderable_block;

}


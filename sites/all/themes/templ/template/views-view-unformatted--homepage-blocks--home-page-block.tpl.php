
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes_array[$id]; ?>">
      <div class="center">
            <?php print $row; ?>
          <a href="#" class="goTop"><?php echo t('Back to top'); ?></a>
      </div>
  </div>
<?php endforeach; ?>

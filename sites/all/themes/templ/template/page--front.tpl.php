<?php require_once ( $directory. "/template/incl/header.phtml"); ?>
<?php if($page['quotes']): ?>
<div id="quotes">
    <div class="center">
        <?php echo render($page['quotes']);?>
    </div>
</div>
<?php endif; ?>
<div id="higlightBlok">
    <div class="center">
        <?php if($page['highlights']): ?>
        <div id="highlights">
            <?php echo render($page['highlights']);?>
        </div>
        <?php endif; ?>
        <div id="highlightContent"><div class="highCont"></div></div>
        <?php if($page['videos']): ?>
        <div id="videos">
            <?php echo render($page['videos']);?>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php if($page['groeiagenda']): ?>
<div id="groeiAgenda" class="textured">
    <div class="center">
        <?php echo render($page['groeiagenda']); ?>
        
    </div>
</div>
<?php endif; ?>


<?php if($page['methode']): ?>
<div id="methode" class="textured">
    <div class="center">
        <?php echo  render($page['methode']); ?>
    </div>
</div>
<?php endif; ?>

<?php if($page['homepage']): ?>
<div id="content">
    <?php echo render($page['homepage']);?>
</div>

<?php endif; ?>

<?php if ($messages): ?>
<div id="messages">
    <?php print $messages; ?>
</div>
<?php endif; ?>

<?php require_once ( $directory. "/template/incl/footer.phtml"); ?>
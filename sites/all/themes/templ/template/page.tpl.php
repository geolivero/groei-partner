<body>
<?php if ($messages): ?>
<div id="messages">
    <?php print $messages; ?>
</div>
<?php endif; ?>
<div id="subContent" class="subItem">
    <div id="contentOnly">
		<h1><?php echo $title;?></h1>
    	<?php echo render($page['content']);?>
	</div>
	<?php if ($type === 'downloads'): ?>
    <?php echo render($download_fun_downloadblock); ?>
	<?php endif; ?>
</div>
</body>
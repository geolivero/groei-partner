<div class="groeiContent">
	<div class="colls">
		<div class="left">
			<div class="date">
				<?php 
					$ge_date = explode(',', $fields['field_date']->content);
					$ge_date_mnth = explode(' ', $ge_date[1]);
					$ag_year = $ge_date[2];

				?>
				<span class="day"><?php echo $ge_date_mnth[2] ?></span>
				<div>
					<span class="month"><?php echo $ge_date_mnth[1] ?></span>
					<span class="year"><?php echo $ge_date[2] ?></span>
				</div>
			</div>
			<?php echo $fields['title']->content; ?>

			<div class="content">
				<?php echo $fields['body']->content; ?>
			</div>

			<div class="social">
	            <span class='st_facebook_hcount' content="halo" st_url="<?php echo $fields['field_link_naar']->content ?>" st_title="<?php echo $fields['title']->content; ?>"  displayText='Facebook'></span>
	            <span class='st_twitter_hcount' content="halo" st_url="<?php echo $fields['field_link_naar']->content ?>" st_title="<?php echo $fields['title']->content; ?>" displayText='Tweet'></span>
	            <span class='st_linkedin_hcount' content="halo" st_url="<?php echo $fields['field_link_naar']->content ?>" st_title="<?php echo $fields['title']->content; ?>" displayText='LinkedIn'></span>
	            <span class='st_plusone_hcount' content="halo" st_url="<?php echo $fields['field_link_naar']->content ?>" st_title="<?php echo $fields['title']->content; ?>" displayText='Google +1'></span>
	        </div>
		</div>
		<div class="right">
			<div class="img">
				<?php echo  $fields['field_afbeelding']->content; ?>
				<i class="lamp"></i>
			</div>
			<div class="btn">
				<a target="_blank" href="<?php echo $fields['field_link_naar']->content ?>"><?php echo t('Signup Now'); ?></a>
			</div>
		</div>
	</div>
</div>	
    app_layout = {
    quotes: function () {
        jQuery('#quotes div.center').append('<div class="left"></div><div class="right"></div>');
    },
    higlight: function () {
        jQuery('div#higlightBlok div.center').append('<div class="asset"></div><div class="shadow"></div>');
    },
    textured: function () {
        jQuery('.textured, .home-page-rows.views-row-even').each(function () {
            jQuery(this).append('<div class="stripesTop"></div><div class="stripesBottom"></div>');
        });
        jQuery('div#downloadWidget').each(function () {
            jQuery(this).parent().append('<div class="topShadow"></div><div class="bottomShadow"></div>')
        });
    },
    recenties: function () {
      var counter = 0, widthLi = 475, anim, total = jQuery('#recentieLijst').find('li').length;
      jQuery('#recentieLijst').append('<a href="#" class="leftArrow">&laquo;</a><a href="#" class="rightArrow">&raquo;</a>');
      jQuery('#recentieLijst ul').width(widthLi * total);
      anim = function () {
          jQuery('#recentieLijst ul').animate({marginLeft: -(widthLi * counter)}, 500);
          
      };
      
      jQuery('#recentieLijst .rightArrow').click(function () {
          counter += 1;
          if (counter > total - 2) {
              jQuery(this).hide();
          }
          jQuery('#recentieLijst .leftArrow').show();
          anim();
          return false;
      });
      
      jQuery('#recentieLijst .leftArrow').hide().click(function () {
          counter -= 1;
          if (counter < 1) {
              jQuery(this).hide();
          }
          jQuery('#recentieLijst .rightArrow').show();
          anim();
          return false;
      });
      
    },
    formObjects: function (form) {
        var formObj = {};
        form.find('input').each(function(){
           if (jQuery(this).attr('type') === 'checkbox') {
               if (jQuery(this).is(':checked')) {
                   formObj[jQuery(this).attr('name')] = jQuery(this).val();
               } else {
                   formObj[jQuery(this).attr('name')] = undefined;
               }
           } else {
               formObj[jQuery(this).attr('name')] = jQuery(this).val();
           }

        });
        return formObj;
    },
    groeipartner: function () {
        jQuery('div').each(function () {
            if (jQuery(this).text().toLowerCase().indexOf('groeipartner') > -1) {
                if (jQuery(this).html().toLowerCase().indexOf('<img') < 1
                    && jQuery(this).html().toLowerCase().indexOf('<a')
                    ) {
                    /*var wrapTitle = jQuery(this).html().replace(/groeipartner/g, '<span>g</span>roei<span>p</span>artner');
                    //jQuery(this).html(wrapTitle);*/
                }
                
            }
        });
        },
    loader: function (dom) {
        if (dom.length > 0) {
            if (jQuery('.loader').length > 0 ) {
                jQuery('.loader').remove();
            }
			 jQuery('.loader').remove();
            dom.append('<img style="z-index: 999;" class="loader" src="' + r + 'images/ajax-loader.gif" />');
        }
        return {
            hide: function () {
                jQuery('.loader').hide();
            },
            show: function () {
                jQuery('.loader').show();
            }
        }

    }
}

app_interactive = {
    quotes: function () {
        var counter = 0, length = jQuery('#quotes div.center ul li').length,
        showQuote, time = 8;
        showQuote = function () {
            counter += 1;
            if (counter > length - 1) {
                counter = 0;
            }
            setTimeout(function () {
                jQuery('#quotes div.center ul li').eq(counter - 1).fadeOut('slow', function () {
                    jQuery('#quotes div.center ul li').eq(counter).fadeIn('slow', function () {
                        showQuote();
                    }); 
                });
                
            }, time * 1000);
             
        };

        
        showQuote();
    },
    youtube: function (settings) {
		
        var dim = settings.dim || {
            width: 420, 
            height: 315
        }, pl = settings.placeHolder || {
            width: 420, 
            height: 315
        }, sluitenButton = window.location.toString().indexOf('/en') > 0 ? 'close' : 'sluiten';
        moreMovies = '', moreMoviesList = '', tumbs = settings.tumbs || {
            width: 80, 
            height: 60
        }, vidTumbs = function () {};
        
        moreMovies = ['<div id="app_youtubeHolder"><ul></ul><a class="sluiten" href="#">' , sluitenButton , '</a></div>'].join('');
        jQuery('.youtube_list li div.videos').each(function (i) {
            var title, vidLink = jQuery(this).text().split('/')[3], youtube = '', current = i === 0 ? 'current': '';
            title = jQuery(this).attr('title').length > 30 ? jQuery(this).attr('title').substring(0, 10) + '...' +  jQuery(this).attr('title').substring(jQuery(this).attr('title').length - 10, jQuery(this).attr('title').length) : jQuery(this).attr('title');
            youtube = '<iframe width="'+ dim.width + '" height="'+ dim.height + '" src="http://www.youtube.com/embed/' + vidLink + '" frameborder="0" allowfullscreen></iframe>';
            jQuery(this).html('<div class="media" style="width: ' + pl.width + 'px; height:' + pl.height + ';"><img src="http://i4.ytimg.com/vi/' + vidLink +  '/0.jpg" height="' + pl.height + '" style="width: ' + pl.width + 'px; height:' + pl.height + ';"/><span class="playBtn"></span></div>');
            jQuery(this).closest('li').addClass(current);
            moreMoviesList += '<li><a title="' + jQuery(this).attr('title') + '" href="#' + vidLink + '"><img src="http://i4.ytimg.com/vi/' + vidLink +  '/1.jpg" width="' + tumbs.width +'" height="' + tumbs.height +'"/><span>' + title + '</span></a></li>';
            jQuery(this).click(function () {
                app_createOwnPopUp.open({
                    prefix: '<div class="ownInfo">',
                    suffix: '</div>',
                    content: youtube
                });
                return false;
            });
        });
        vidTumbs = function () {

            jQuery('#app_youtubeHolder li a').click(function () {
              
                var youtube = '', vidLink = jQuery(this).attr('href').replace('#', '');
                youtube = '<div id="youtubeVidHolder"><iframe width="'+ dim.width + '" height="'+ dim.height + '" src="http://www.youtube.com/embed/' + vidLink + '" frameborder="0" allowfullscreen></iframe></div>';
                jQuery('#app_youtubeHolder').fadeOut('slow', function (){
                    app_createOwnPopUp.changeContent(youtube  + '<a class="terugToClibs" href="#">terug</a>');
                    jQuery('.terugToClibs').click(function () {
                        jQuery('#youtubeVidHolder').fadeOut('slow', function () {
                            app_createOwnPopUp.changeContent('<div id="app_youtubeHolder"><ul>' + moreMoviesList + '</ul></div>');
                            jQuery('#app_youtubeHolder').hide().fadeIn('slow');
                            vidTumbs();
                        });
                        return false; 
                    });
                });
              
                return false;
            });
          
          
        };
        if (jQuery('.youtube_list li').length > 0) {
            jQuery('.meerInfos').click(function () {
                app_createOwnPopUp.open({
                    prefix: '<div id="app_youtubeHolder"><ul>',
                    suffix: '</ul></div>',
                    content: moreMoviesList
                });
                vidTumbs();
                return false;
            });
        }
        
    },
    smallInfo: function () {
        jQuery('div#highlights a').click(function () {
			jQuery('#highlightContent div.highCont').html('');
            var loader = app_layout.loader(jQuery('#highlightContent div.highCont')), closeFunc;

            closeFunc = function () {
                jQuery('#highlightContent .closeButton').click(function () {
                    jQuery('#highlightContent').animate({width: 0}, 500);
                    jQuery('#videos').animate({right: 0}, 500, function () {
                         jQuery('#highlightContent div.highCont').html('');
                    });
                    jQuery(this).hide();
                    return false;
                });
            };

            jQuery('#highlightContent').animate({width: 305}, 500);
            jQuery('#videos').animate({right: -305}, 500);
            
            jQuery('#highlightContent div.highCont').load(jQuery(this).attr('href') + ' #subContent', function () {
				sluitenButton = window.location.toString().indexOf('/en') > 0 ? 'close' : 'sluiten';
                loader.hide();
                jQuery('#highlightContent div.highCont').append(['<a class="closeButton" href="#">' , sluitenButton , '</a>'].join(''));
                closeFunc();
            });
            return false;
        });
    },
    methods: function () {
        jQuery('div.stepsMethod').each(function (i) {
            var index = (i + 1).toString(), steps = jQuery(this).html().replace(/\s+/g, ' ').replace(index, '<span>' + index + '</span>');
            jQuery(this).html(steps);
        });
        jQuery('div.bodyMethod').each(function () {
            jQuery(this).prepend('<div class="graphic"></div>'); 
        });
        
        jQuery('div.stepsMethod').click(function () {
            jQuery(this).closest('ul').find('li').each(function () {
                jQuery(this).removeClass('current').removeClass('views-row-first');
                if (jQuery(this).width() > 300) {
                    jQuery(this).stop().animate({
                        width: 120
                    }, 1000);
                }
            });
            jQuery(this).closest('li').addClass('current');
            jQuery(this).closest('li').stop().animate({
                width: 400
            }, 1000);
        });
        jQuery('div[title="Methode"]').html('');
    },
    pageAnimations: function () {
        jQuery('.goTop').click(function () {
            jQuery('html, body').animate({scrollTop: 0}, 1000);
            return false; 
        });
        
        jQuery('#navigation a').click(function () {
            var domPos = jQuery(this).text().toLowerCase() === 'home' ? 0
            : jQuery(".home-page-rows div[title='" + jQuery(this).text() +"']").offset().top - 195;
            if (jQuery(this).text() === 'Methode' || jQuery(this).text() === 'Method') {
		console.log(domPos);
                jQuery('html, body').animate({scrollTop: domPos - 440}, 1000);
            } else {
                jQuery('html, body').animate({scrollTop: domPos}, 1000);
            }
            
            jQuery('#navigation a').closest('li').removeClass('active');
            jQuery(this).closest('li').addClass('active');
            
            return false; 
        });
    },
    downloads: function () {
        var loadMore, counter = 0, handleForm, oldHtml = '', loader = app_layout.loader(jQuery('#downloadWidget').parent());
        loader.show();
        if (jQuery('#downloadWidget').length > 0) {
            jQuery('div.image a.loadResults').click(function () {
                loadMore();
                return false;
            });
            jQuery('#downloadWidget').append('<ul class="prodList"></ul>');
        }

        jQuery('#downloadWidget ul.prodList a').live('click', function () {
                app_createOwnPopUp.open({
                    prefix: '<div class="downloadInfoContent">',
                    suffix: '</div>',
                    content: '<iframe frameborder="0" src="' + jQuery(this).attr('href') + '"></iframe>'
                });

				jQuery('#ownPop .ownContent .ownContentHolder .file a').attr('target', '_blank');
                app_createOwnPopUp.setHeight(460);
            
            return false;
        });
        loadMore =  function () {
			
            if (jQuery('#downloadWidget').length > 0) {
                loader.show();
				linkTaal = window.location.toString().split('/')[3] !== 'en' ? 'nl' : 'en';
                jQuery.get('/' + linkTaal + '/downloads-items?page=' + counter, function (data) {
                    var ul = jQuery(data).find('div.item-list ul');
                    if (!ul.find('li').length) {
                        jQuery('div.image a.loadResults').hide();
                    }
                    jQuery('#downloadWidget ul').append(ul.find('li'));
                    jQuery('#downloadWidget').scrollTop(jQuery('#downloadWidget ul').height());
                    loader.hide();
                });
            }
            counter += 1;
        }
        loadMore();
        
    }

}


app_createOwnPopUp = {
    open: function (o) {

        var html = "<div id=\"ownPop\"><div class=\"ownOverlay\"></div><div class=\"ownContent\"></div></div>",
        _this = this, $ = jQuery, sluitenButton = window.location.toString().indexOf('/en') > 0 ? 'close' : 'sluiten';
        $('body').append(html);
        $('#ownPop').hide();
        $('#ownPop .ownContent').hide();
        $('#ownPop .ownContent').append(['<div class="ownControllers"><a href="#">' , sluitenButton , '</a></div>'].join(''));
        $('#ownPop .ownContent').append('<div class="ownContentHolder"></div>');
        if (o.content !== undefined) {
            $('#ownPop .ownContent .ownContentHolder').append(o.prefix + o.content + o.suffix);
        }

        $('#ownPop .ownOverlay, #ownPop div.ownControllers a').click(function () {
            _this.close();
            return false;
        });

        $('#ownPop').fadeIn('slow', function () {
            $('#ownPop .ownContent').fadeIn('slow');
        });

        $(window).scroll(function () {
            var top =  $(window).scrollTop() + ($(window).height() / 2);
            $('#ownPop .ownContent').css({
                top: top
            });
        });

        $('#ownPop .ownContent').css({
            top:  $(window).scrollTop() + ($(window).height() / 2)
            });
    },
    close: function () {
        var $ = jQuery;
        $('#ownPop .ownContent').fadeOut('slow', function () {
            $('#ownPop').fadeOut('slow', function () {
                $('#ownPop').remove();
            });
        });
    },
    setVideoProps: function (width, height) {
        var $ = jQuery;
        $('#ownPop iframe').width(width);
        $('#ownPop iframe').height(height);
    },
    setHeight: function (height) {;
        jQuery('div.ownContent').height(height);
        jQuery('div.ownContent').css({marginTop : -(height / 2)});
    },
    changeContent: function (content) {
        var $ = jQuery;
        $('#ownPop .ownContent .ownContentHolder').html(content);
    }
};


app_layout.selectAgendas = {

    select: function () {
        var $ = jQuery, openAgenda;

        openAgenda = function () {
            $('.agendaItems ul li').addClass('hidden');
            $('.agendaItems ul li').removeClass('active');
            $('.agendaItems ul li').eq($(this).index()).addClass('active').removeClass('hidden');
            $('.selector ul li').removeClass('active');
            $(this).closest('li').addClass('active');
            $('#metaIMG').attr('content', $('.agendaItems ul li').eq($(this).index()).find('img').attr('src'));
        };
        $('#agendaTitle').each(function () {
            $(this).html('<span>' + $(this).text().substring(0, 1) + '</span>' + $(this).html().substring(1,$(this).html().length));
        });
        $('.selector ul li').click(openAgenda);
    },
    init: function () {
        this.select();
    }
}


jQuery(function () {
	jQuery('div[title="Method"]').css({ position: 'absolute', left: -3000});
	
    app_layout.quotes();
    app_interactive.quotes();
    app_layout.textured();
    app_layout.groeipartner();
    app_interactive.youtube({
        dim: {
            width: 600, 
            height: 360
        },
        placeHolder: {
            width: 265, 
            height: 200
        },
        tumbs: {
            width: 80, 
            height: 60
        }
    });
    app_interactive.smallInfo();
    app_interactive.methods();
    app_interactive.pageAnimations();
    app_layout.higlight();
    app_layout.recenties();
    app_interactive.downloads();
    if (jQuery('#messages').length > 0) {
        jQuery('body, html').scrollTop(jQuery('#messages').position().top + 50);
    }
    app_layout.selectAgendas.init();
    
});



(function($) {

    $.fn.easyTooltip = function(options){

        // default configuration properties
        var defaults = {
            xOffset: 10,
            yOffset: 25,
            tooltipId: "easyTooltip",
            tooltipClass: 'easyTip',
            clickRemove: false,
            content: "",
            useElement: ""
        };

        var options = $.extend(defaults, options);
        var content;
		
        this.each(function() {
            var title = $(this).attr("title");
            $(this).hover(function(e){
                content = (options.content != "") ? options.content : title;
                content = (options.useElement != "") ? $("#" + options.useElement).html() : content;
                $(this).attr("title","");
                if (content != "" && content != undefined){
                    $("body").append("<div class=\"" + options.tooltipClass + "\" id='"+ options.tooltipId +"'><div class=\"bottom\">"+ content +"</div><div class=\"right\"></div><div class=\"tip\"></div></div>");
                    $("#" + options.tooltipId)
                    .css("position","absolute")
                    .css("top",(e.pageY - options.yOffset) + "px")
                    .css("left",(e.pageX + options.xOffset) + "px")
                    .css("display","none")
                    .fadeIn("fast")
                }
            },
            function(){
                $("#" + options.tooltipId).remove();
                $(this).attr("title",title);
            });
            $(this).mousemove(function(e){
                $("#" + options.tooltipId)
                .css("top",(e.pageY - options.yOffset) + "px")
                .css("left",(e.pageX + options.xOffset) + "px")
            });
            if(options.clickRemove){
                $(this).mousedown(function(e){
                    $("#" + options.tooltipId).remove();
                    $(this).attr("title",title);
                });
            }
        });

    };

})(jQuery);

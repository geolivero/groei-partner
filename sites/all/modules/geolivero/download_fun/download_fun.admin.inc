<?php

function download_fun_list() {
    $count = count(download_fun_fetch_data(TRUE));




    $headers = array(
        'name' => array('data' => t('Name'), 'field' => 'df.dpname'),
        'surname' => array('data' => t('Surname'), 'field' => 'df.dpsurname'),
        'street' => t('Street and number'),
        'city' => array('data' => t('City'), 'field' => 'df.dpplaats'),
        'email' => array('data' => t('Email'), 'field' => 'df.dpemail'),
        'phone' => t('Phone number'),
        'date' => array('data' => t('Date'), 'field' => 'df.dpdatum'),
        'download' => t('Download'),
		'acties' => t('Acties'),
    );

    $query = db_select('download_fun', 'df')
                    ->extend('PagerDefault')->extend('TableSort')
                    ->fields('df')
                    ->limit(50)
                    ->orderByHeader($headers);
    $result = $query->execute();

    $options = array();
    foreach ($result as $downFun) {
        $options[$downFun->dpid] = array(
            'name' => $downFun->dpname,
            'surname' => $downFun->dpsurname,
            'street' => $downFun->dpstreet . ' ' . $downFun->dpnumber,
            'city' => $downFun->dpplaats,
            'email' => $downFun->dpemail,
            'phone' => $downFun->dptelefoon,
            'date' => $downFun->dpdatum,
            'download' => get_node_link($downFun->nid),
			'acties' => l(t('Delete'), 'admin/gebruikers/delete/' . $downFun->dpid),
        );
    }
    $form['gebruiker'] = array(
        '#type' => 'markup',
        '#markup' => '<h2>' . t('Aantal downloads') . '</h2><p>Totaal gedownloade bestanden (' . $count . ').</p>',
    );
    $form['list'] = array(
        '#type' => 'tableselect',
        '#header' => $headers,
        '#options' => $options,
        '#empty' => t('No content available.'),
    );
    $form['pager'] = array('#markup' => theme('pager'));

    return $form;
}



function get_list_user_down () {
    query_to_csv();
}


function download_fun_confirm_delete ($form, &$form_state, $id) {

	$form['iddownload'] = array(
			'#type' => 'value',
			'#value' => $id,
		);
		return confirm_form($form,
			t('Are you sure you want to delete this download info?'),
			'admin/structure/gebruikers',
			t('This action cannot be undone.'),
			t('Delete'),
			t('Cancel'));
}

function download_fun_confirm_delete_submit ($form, &$form_state) {
	$form_values = $form_state['values'];
	if ($form_state['values']['confirm']) {
		$id = $form_state['values']['iddownload'];
		drupal_set_message(t("The information has been deleted!"));
		$num_deleted = db_delete('download_fun')
		  ->condition('dpid', $id)
		  ->execute();
	}
	drupal_goto("admin/structure/gebruikers");
}

function download_fun_fetch_data($all = FALSE) {
    $results = array();
    $query = db_select('download_fun', 'df')
                    ->fields('df')
                    ->orderBy('df.dpdatum', 'ASC');

    $result = $query->execute();
    while ($record = $result->fetchAssoc()) {
        $results[] = $record;
    }

    return $results;
}

function get_node_link($nid) {
    $node = node_load($nid);
    $languages = language_list();
    $l_options = $node->language != LANGUAGE_NONE && isset($languages[$node->language]) ? array('language' => $languages[$node->language]) : array();
    $nodeLink = array(
        'data' => array(
            '#type' => 'link',
            '#title' => $node->title,
            '#href' => 'node/' . $node->nid,
            '#options' => $l_options,
            '#suffix' => ' ' . theme('mark', array('type' => node_mark($node->nid, $node->changed))),
        )
    );
    return $nodeLink;
}

function query_to_csv() {



    header('Content-Type: text/csv');
    header('Content-Disposition: attachment;filename=gebruikers.csv');
    $fp = fopen('php://output', 'w');
    $header = array();
    $header[] = array(
        'id', 'iddownload', 'naam', 'achternaam',
        'straat', 'huisnummer', 'postcode', 'plaats',
        'telefoon', 'email', 'datum',
    );
    $datas = download_fun_fetch_data();

    foreach($header as $data) {
        fputcsv($fp, $data);
    }
    foreach($datas as $data) {
        fputcsv($fp, $data);
    }

    fclose($fp);
}